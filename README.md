# CSC Milter
This is a milter ("mail filter") for CSC.
It will reject a message if and only if the following conditions are satisfied:

* a message arrives on port 25 or 465
* the client is not authenticated
* the From: header (**not** the envelope header) contains a CSC address
* there is no Received: header indicating that the message was previously received by a CSC mail server

The objective is to reject messages from spammers spoofing the From: header
to a CSC address.

The last point (the Received header check) is to allow forwarding and mailing lists (e.g. a CSC member sends an email to the DSA mailing list, to which the syscom list is subscribed). This isn't perfect because the Received header can also be spoofed, but it should probably be good enough for now.

## Installation
As root:
```
apt install python3-milter
pip3 install .
```
Installing python3-milter will also install libmilter as a dependency.

Now open /etc/postfix/main.cf and add 'unix:/run/csc-milter/csc-milter.sock'
to the end of smptd_milters. Example:
```
smtpd_milters = unix:/var/spool/postfix/spamass/spamass.sock unix:/run/csc-milter/csc-milter.sock
```
Also open /etc/postfix/master.cf and explicitly set smtpd_milters *without*
csc-milter for the ports where csc-milter should not be running. Example:
```
submission inet n       -       n       -       -       smtpd
  -o smtpd_sasl_auth_enable=yes
  ...
  -o smtpd_milters=unix:/var/spool/postfix/spamass/spamass.sock
  ...
```
Notice how smtpd_milters above does not have the csc-milter socket path. Therefore
csc-milter will not be invoked on messages arriving on port 587 (submission).

You also need to modify milter\_connect\_macros in
/etc/postfix/main.cf to include `{daemon_port}` and `{auth_type}`. This allows
authenticated cilents to be skipped.
Example:
```
milter_connect_macros = j v _ {daemon_name} {daemon_port} {auth_type}
```

Optional: add the following to /etc/postfix/main.cf:
```
smtpd_milter_maps = cidr:/etc/postfix/smtpd_milter_map
```
Then, in /etc/postfix/smtpd\_milter\_map, add something like the following:
```
127.0.0.0/8    DISABLE
192.168.0.0/16 DISABLE
::/64          DISABLE
2001:db8::/32  DISABLE
```
This ensures that csc-milter will not be run on messages from local clients.
Replace 'DISABLE' by any additional milters which should be run. Note that
even if you do not do this, csc-milter will still accept messages from local
clients.

### Running locally
For development purposes:
```
su -s /bin/bash postfix
python3 csc_milter/main.py
```

### systemd
The Debian package installs a systemd service named csc-milter. Usage:
```
systemctl enable csc-milter
systemctl start csc-milter
```

## Tests
Run the following from the root directory:
```
pip3 install -r test_requirements.txt
pytest
```
