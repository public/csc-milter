import ipaddress
import re
import socket
from unittest.mock import Mock

import Milter

import csc_milter.main as csc_milter


def setup_module():
    csc_milter.MYNETWORKS = [
        ipaddress.ip_network('129.97.134.0/24'),
    ]
    csc_milter.MYORIGIN = 'csclub.uwaterloo.ca'
    csc_milter.RECEIVED_REGEX = re.compile(r"\bby ([\w-]+\.)?csclub\.uwaterloo\.ca\b")


def teardown_module():
    csc_milter.MYNETWORKS = []
    csc_milter.MYORIGIN = ''
    csc_milter.RECEIVED_REGEX = None


def get_milter(dst_port, auth=False):
    milter = csc_milter.CSCMilter()
    def getsymval(s):
        if s == '{daemon_port}':
            return str(dst_port)
        if s == '{auth_type}' and auth:
            return 'PLAIN'
        return None
    milter.getsymval = Mock(side_effect=getsymval)
    milter._protocol = 18266  # to make the @Milter.noreply work
    milter.setreply = Mock()
    return milter


def test_local_port_25():
    milter = get_milter(25)
    ret = milter.connect('caffeine.csclub.uwaterloo.ca', socket.AF_INET, ('129.97.134.17', 10000))
    assert ret == Milter.ACCEPT


def test_local_port_587():
    milter = get_milter(587)
    ret = milter.connect('caffeine.csclub.uwaterloo.ca', socket.AF_INET, ('129.97.134.17', 10000))
    assert ret == Milter.ACCEPT


def remote_connect(milter):
    return milter.connect('[24.114.29.182]', socket.AF_INET, ('24.114.29.182', 10000))


def test_remote_port_25_csc_address():
    milter = get_milter(25)
    ret = remote_connect(milter)
    assert ret == Milter.CONTINUE
    milter.envfrom('<user@example.com>')
    ret = milter.header('From', '<syscom@csclub.uwaterloo.ca>')
    assert ret == Milter.REJECT


def test_remote_port_25_non_csc_address():
    milter = get_milter(25)
    remote_connect(milter)
    milter.envfrom('<user@example.com>')
    ret = milter.header('From', 'John Doe <user@example.com>')
    assert ret == Milter.CONTINUE


def test_remote_port_587():
    milter = get_milter(587)
    ret = remote_connect(milter)
    assert ret == Milter.ACCEPT


def test_remote_port_25_with_auth():
    milter = get_milter(25, auth=True)
    remote_connect(milter)
    ret = milter.envfrom('<user@csclub.uwaterloo.ca>')
    assert ret == Milter.ACCEPT


def test_remote_port_25_with_received_origin():
    milter = get_milter(25)
    remote_connect(milter)
    milter.envfrom('<user@csclub.uwaterloo.ca>')
    ret = milter.header('Received', 'from 24.114.29.182\n\tby mail.csclub.uwaterloo.ca (Postfix)')
    assert ret == Milter.ACCEPT


def test_remote_port_25_with_received_nonorigin():
    milter = get_milter(25)
    remote_connect(milter)
    milter.envfrom('<user@csclub.uwaterloo.ca>')
    ret = milter.header('Received', 'from 24.114.29.182\n\tby mail.example.com (Postfix)')
    assert ret == Milter.CONTINUE
    ret = milter.header('From', '<syscom@csclub.uwaterloo.ca>')
    assert ret == Milter.REJECT
