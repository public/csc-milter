import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand

requirements = [line.strip() for line in open('requirements.txt')]
test_requirements = [line.strip() for line in open('test_requirements.txt')]
long_description = open('README.md').read()


class PyTest(TestCommand):
    def run_tests(self):
        import pytest
        sys.exit(pytest.main([]))


setup(
    name='csc-milter',
    version='0.2.2',
    description='Custom milter for CSC',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://git.csclub.uwaterloo.ca/public/csc-milter.git',
    author='CSC Systems Committee',
    author_email='syscom@csclub.uwaterloo.ca',
    classifiers=[
        'Programming Language :: Python :: 3',
        'OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: Unix',
    ],
    license='GPLv3',
    keywords='email, milter',
    packages=find_packages(),
    python_requires='>=3.6',
    install_requires=requirements,
    tests_require=test_requirements,
    cmdclass={'test': PyTest},
    entry_points={
        'console_scripts': ['csc-milter=csc_milter.main:main']
    },
)
